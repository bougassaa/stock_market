package montp.tasks;

import montp.api.Quote;
import montp.api.StockMarketClient;
import montp.data.model.QuoteCart;
import montp.data.model.QuoteHistory;
import montp.data.model.security.User;
import montp.services.QuoteCartService;
import montp.services.QuoteHistoryService;

import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

@Singleton
@Startup
@LocalBean
public class RetrieveQuotes {

    @Inject
    QuoteCartService serviceCart;
    @Inject
    QuoteHistoryService serviceHistory;
    @Inject
    StockMarketClient api;

    @Schedule(hour = "*", minute = "*/5", persistent = false)
    public void process() {
        Map<String, Double> quoting = new HashMap<>();

        // get distinct symbol for all carts
        serviceCart.getSymbols().forEach(symbol -> {
            Quote quote = api.getQuote(symbol);
            quoting.put(symbol, quote.getQuote());
        });

        // get carts and group by users
        Map<User, List<QuoteCart>> carts = serviceCart.getAll()
            .stream()
            .collect(Collectors.groupingBy(QuoteCart::getUser));

        Date date = new Date();
        carts.forEach(((user, quoteCarts) -> {
            List<Double> variations = new ArrayList<>();
            List<Double> quotes = new ArrayList<>();

            // calculate quotes
            for (QuoteCart cart : quoteCarts) {
                Double q = quoting.get(cart.getCompanySymbol());
                variations.add(((q / cart.getInitialQuote()) * 100) - 100);
                quotes.add(q);
            }

            // insert into history table
            serviceHistory.insert(new QuoteHistory(
                user,
                date,
                quotes.stream()
                    .mapToDouble(a -> a)
                    .average()
                    .orElse(0),
                variations.stream()
                    .mapToDouble(a -> a)
                    .average()
                    .orElse(0)
            ));
        }));
    }

}
