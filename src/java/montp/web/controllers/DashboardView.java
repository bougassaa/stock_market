package montp.web.controllers;

import montp.api.Company;
import montp.api.Quote;
import montp.api.StockMarketClient;
import montp.data.model.QuoteCart;
import montp.data.model.QuoteHistory;
import montp.services.QuoteCartService;
import montp.services.QuoteHistoryService;
import montp.tools.Tools;
import montp.web.FacesTools;
import montp.web.UserSession;
import montp.web.adapters.QuoteCartAdapter;
import org.primefaces.PrimeFaces;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.line.LineChartDataSet;
import org.primefaces.model.charts.line.LineChartModel;
import org.primefaces.model.charts.line.LineChartOptions;
import org.primefaces.model.charts.optionconfig.animation.Animation;
import org.primefaces.model.charts.optionconfig.title.Title;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@ViewScoped
@Named("dashboard")
public class DashboardView extends View {

    @Inject
    StockMarketClient api;
    @Inject
    QuoteCartService quoteCartService;
    @Inject
    QuoteHistoryService quoteHistoryService;
    @Inject
    UserSession session;

    Collection<Quote> randomQuotes;

    private LineChartModel cotationChart;
    private LineChartModel variationChart;

    public Collection<Quote> getRandomQuotes() {
        return randomQuotes;
    }

    @PostConstruct
    public void init() {
        initCharts();
        initRandomQuotes();
    }

    private void initRandomQuotes() {
        Collection<Company> companies = api.getCompanies();

        Set<String> symbols = new HashSet<>();

        for (Company company : companies) {
            symbols.add(company.getSymbol());
        }
        // todo : make sort random
        randomQuotes = api.getQuotes(symbols)
                .stream()
                .limit(30)
                .collect(Collectors.toList());
    }

    public void initCharts() {
        cotationChart = new LineChartModel();
        variationChart = new LineChartModel();

        List<Object> quotingValues = new ArrayList<>();
        List<Object> variationValues = new ArrayList<>();
        List<String> labels = new ArrayList<>();

        List<QuoteHistory> histories = quoteHistoryService.getLastHour();

        histories.forEach(quote -> {
            quotingValues.add(Tools.round(quote.getQuote(), 2));
            variationValues.add(Tools.round(quote.getVariation(), 2));
            labels.add(Tools.formatDate(quote.getDateTime(), "HH:mm"));
        });

        // todo : manage translation
        setChartBehavior(cotationChart, quotingValues, labels, "Cotation moyenne de la dernière heure", "Cotation", "rgb(75, 192, 192)");
        setChartBehavior(variationChart, variationValues, labels, "Variation moyenne de la dernière heure", "Variation", "rgb(181 114 221)");
    }

    private void setChartBehavior(LineChartModel chart, List<Object> values, List<String> labels, String title, String label, String color) {
        ChartData data = new ChartData();
        LineChartDataSet dataSet = new LineChartDataSet();

        dataSet.setData(values);
        dataSet.setFill(false);
        dataSet.setLabel(label);
        dataSet.setBorderColor(color);
        dataSet.setLineTension(0.1);

        data.addChartDataSet(dataSet);
        data.setLabels(labels);

        //Options
        LineChartOptions options = new LineChartOptions();
        Title t = new Title();
        t.setDisplay(true);
        t.setText(title);
        options.setTitle(t);

        // disable animation
        Animation animation = new Animation();
        animation.setDuration(0);
        options.setAnimation(animation);

        chart.setOptions(options);
        chart.setData(data);
    }

    public List<QuoteCartAdapter> getQuoteCart() {
        return quoteCartService.getQuoteCart();
    }

    public LineChartModel getCotationChart() {
        return cotationChart;
    }

    public LineChartModel getVariationChart() {
        return variationChart;
    }

    public Double getAverageQuote() {
        List<Double> quotes = new ArrayList<>();

        quoteCartService.getQuoteCart().forEach(adpater -> quotes.add(adpater.getQuote()));

        return quotes.stream()
            .mapToDouble(a -> a)
            .average()
            .orElse(0);
    }

    public Double getAverageVariation() {
        List<Double> variations = new ArrayList<>();

        quoteCartService.getQuoteCart().forEach(adpater -> variations.add(adpater.getVariation()));

        return Tools.round(variations.stream()
            .mapToDouble(a -> a)
            .average()
            .orElse(0), 2);
    }

    public Boolean isNegativeVariation() {
        return getAverageVariation() <= 0;
    }

    public void addQuote(Quote quote) {

        quoteCartService.insert(new QuoteCart(
            quote.getCompany().getSymbol(),
            quote.getQuote(),
            session.getUser()
        ));

        FacesTools.addMessage(FacesMessage.SEVERITY_INFO, "Société ajoutée");

        // update front-end
        PrimeFaces.current().ajax().update("dashBoardForm");
        PrimeFaces.current().ajax().update("growl");
        PrimeFaces.current().executeScript("initTextFit()");
    }
}
