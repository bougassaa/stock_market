package montp.tasks;

import montp.data.model.QuoteCart;
import montp.data.model.security.User;
import montp.services.QuoteCartService;
import montp.services.QuoteHistoryService;
import montp.tools.EMailer;

import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.mail.MessagingException;
import java.util.List;

@Singleton
@Startup
@LocalBean
public class SendQuoteByMail {

    @Inject
    QuoteCartService serviceCart;
    @Inject
    QuoteHistoryService serviceHistory;
    @Inject
    EMailer eMailer;

    @Schedule(hour = "*", minute = "*/15", persistent = false)
    public void process() {
        List<QuoteCart> carts = serviceCart.getAll();

        for (QuoteCart cart : carts) {
            User user = cart.getUser();

            // put condition below (to be finished)
            if (false && user.getIsActive()) {
                try {
                    eMailer.send(user.getUserName(), "Quotation en cours", "");
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
