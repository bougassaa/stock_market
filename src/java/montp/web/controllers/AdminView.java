package montp.web.controllers;

import montp.data.dao.UserDAO;
import montp.data.model.security.User;
import montp.services.UserService;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@ViewScoped
@Named("admin")
public class AdminView extends AbstractManyObjectsView<User, UserDAO, UserService> {

    @PostConstruct
    public void init() {
        setObjects(service.getUsers());
    }

    public void save(User user) {
        service.update(user);
    }
}
