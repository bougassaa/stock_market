package montp.web.controllers.user;

import montp.data.model.security.User;
import montp.web.FacesTools;
import montp.web.UserSession;

import javax.annotation.PostConstruct;
import javax.el.MethodExpression;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import javax.transaction.TransactionalException;
import java.sql.SQLException;

@ViewScoped
@Named("signin")
public class SigninView extends UserView {

    @PostConstruct
    public void init() {
        object = new User();
    }

    public void save() {
        if (service.checkIfExists(object.getUserName())) {
            FacesTools.addMessage(FacesMessage.SEVERITY_ERROR, messages.get("user.duplicate"));
        } else {
            service.insert(object);
            FacesTools.addFlashMessage(FacesMessage.SEVERITY_INFO, String.format(messages.get("user.created"), object.getUserName()));
            this.redirect("user/login");
        }
    }
}
