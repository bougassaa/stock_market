package montp.web.controllers.user;

import montp.data.dao.UserDAO;
import montp.data.model.security.User;
import montp.services.UserService;
import montp.web.UserSession;
import montp.web.controllers.AbstractSingleObjectView;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

@ViewScoped
@Named("user")
public class UserView extends AbstractSingleObjectView<User, UserDAO, UserService> {

}
