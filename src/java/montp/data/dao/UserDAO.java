package montp.data.dao;

import montp.tools.Tools;
import montp.data.model.security.Group;
import montp.data.model.security.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class UserDAO extends GenericDAO<User> {

    public UserDAO() {
        super(User.class);
    }

    @SuppressWarnings("unchecked")
    public List<User> getUsers() {
        return em.createQuery("SELECT u FROM User u JOIN u.groups g WHERE g.groupName <> 'ADMIN' ORDER BY u.userName")
                .getResultList();
    }

    public User getFromUsername(String username) {
        return (User) em.createQuery("SELECT u FROM User u WHERE u.userName=:username")
                .setParameter("username", username)
                .getSingleResult();
    }

    public Group getGroup(String groupname) {
        return em.find(Group.class, groupname);
    }

    @Override
    public void insert(User user) {
        handlePassword(user);
        super.insert(user);
    }

    @Transactional
    public void update(User user) {
        em.createNativeQuery("DELETE FROM SECURITY_USER_GROUP WHERE username=?1")
                .setParameter(1, getUserName(user.getId()))
                .executeUpdate();

        handlePassword(user);
        super.update(user);

        em.createNativeQuery("INSERT INTO SECURITY_USER_GROUP(username,groupname) VALUES(?1,?2)")
                .setParameter(1, user.getUserName())
                .setParameter(2, "USER")
                .executeUpdate();
    }

    private String getUserName(Long id) {
        return (String) em.createQuery("SELECT u.userName FROM User u WHERE u.id=:id")
                .setParameter("id", id)
                .getSingleResult();
    }

    private void handlePassword(User user) {
        if (user.getId() != null) {
            User u = this.get(user.getId());
            // if password hasn't been changed
            if (user.getPassword().isEmpty()) {
                user.setPassword(u.getPassword());
                return;
            }

            if (user.getPassword().equals(u.getPassword()) &&
                    user.getConfirmPassword().isEmpty()) {
                return;
            }
        }

        String pwd = user.getConfirmPassword().isEmpty() ? user.getPassword() : user.getConfirmPassword();
        user.setPassword(Tools.digestSHA256Hex(pwd.trim()));
    }

}
