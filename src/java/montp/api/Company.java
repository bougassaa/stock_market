package montp.api;

import java.io.Serializable;


public class Company implements Serializable {
    
    private String symbol;
    private String name;
    
    public Company() { }

    public Company(String name, String symbol) {
        this.symbol = symbol;
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }

    @Override
    public String toString() {
        return name;
    }

}
