package montp.services;

import montp.data.dao.QuoteHistoryDAO;
import montp.data.model.QuoteHistory;
import montp.web.UserSession;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.*;

@ApplicationScoped
public class QuoteHistoryService extends GenericService<QuoteHistory, QuoteHistoryDAO>{

    @Inject
    UserSession userSession;

    public List<QuoteHistory> getLastHour() {
        return dao.getLastHour(userSession.getUser());
    }

    public Double getAverageQuote() {
        return dao.getAverageQuote(userSession.getUser());
    }

    public Double getAverageVariation() {
        return dao.getAverageVariation(userSession.getUser());
    }
}
