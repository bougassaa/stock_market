package montp.data.dao;

import montp.data.model.QuoteCart;
import montp.data.model.security.User;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class QuoteCartDAO extends GenericDAO<QuoteCart> {

    public QuoteCartDAO() {
        super(QuoteCart.class);
    }

    public List<QuoteCart> getQuoteCart(User user) {
        return em.createQuery("SELECT q FROM QuoteCart q WHERE q.user = :user")
                .setParameter("user", user)
                .getResultList();
    }

    public List<String> getSymbols() {
        return em.createQuery("SELECT DISTINCT q.companySymbol FROM QuoteCart q")
                .getResultList();
    }

    public List<QuoteCart> getAll() {
        return em.createQuery("SELECT q FROM QuoteCart q")
                .getResultList();
    }
}
