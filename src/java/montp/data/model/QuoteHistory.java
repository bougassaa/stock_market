package montp.data.model;

import montp.data.model.security.User;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "QUOTE_HISTORY")
public class QuoteHistory extends GenericEntity{

    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;
    private Date dateTime;
    private Double quote;
    private Double variation;

    public QuoteHistory() {}

    public QuoteHistory(User user, Date dateTime, Double quote, Double variation) {
        this.user = user;
        this.dateTime = dateTime;
        this.quote = quote;
        this.variation = variation;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public Double getQuote() {
        return quote;
    }

    public void setQuote(Double quote) {
        this.quote = quote;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Double getVariation() {
        return variation;
    }

    public void setVariation(Double variation) {
        this.variation = variation;
    }

}
