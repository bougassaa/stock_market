package montp.web.adapters;

import montp.api.Quote;
import montp.data.model.QuoteCart;
import montp.tools.Tools;

public class QuoteCartAdapter {

    private final Quote quote;
    private final QuoteCart cart;

    public QuoteCartAdapter(Quote quote, QuoteCart cart) {
        this.quote = quote;
        this.cart = cart;
    }

    public Double getQuote() {
        return Tools.round(quote.getQuote(), 2);
    }

    public String getSymbol() {
        return quote.getCompany().getSymbol();
    }

    public String getCompany() {
        return quote.getCompany().getName();
    }

    public Double getVariation() {
        return Tools.round(((quote.getQuote() / cart.getInitialQuote()) * 100) - 100, 2);
    }
}
