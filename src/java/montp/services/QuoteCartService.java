package montp.services;

import montp.api.Quote;
import montp.api.StockMarketClient;
import montp.data.dao.QuoteCartDAO;
import montp.data.model.QuoteCart;
import montp.web.UserSession;
import montp.web.adapters.QuoteCartAdapter;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.*;

@ApplicationScoped
public class QuoteCartService extends GenericService<QuoteCart, QuoteCartDAO> {

    @Inject
    StockMarketClient api;
    @Inject
    UserSession userSession;

    public void saveCart(Set<String> symbols) {
        // get quotes for each selected company
        Collection<Quote> quotes = api.getQuotes(symbols);

        quotes.forEach(quote -> {
            // insert quote entity
            this.insert(new QuoteCart(
                quote.getCompany().getSymbol(),
                quote.getQuote(),
                userSession.getUser()
            ));
        });
    }

    public List<QuoteCartAdapter> getQuoteCart() {
        List<QuoteCartAdapter> quoteCart = new ArrayList<>();

        dao.getQuoteCart(userSession.getUser()).forEach(cart -> {
            Quote client = api.getQuote(cart.getCompanySymbol());
            quoteCart.add(new QuoteCartAdapter(client, cart));
        });

        return quoteCart;
    }

    public List<String> getSymbols() {
        return dao.getSymbols();
    }

    public List<QuoteCart> getAll() {
        return dao.getAll();
    }
}
