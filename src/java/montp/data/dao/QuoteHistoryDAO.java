package montp.data.dao;

import montp.data.model.QuoteHistory;
import montp.data.model.security.User;
import montp.tools.Tools;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@ApplicationScoped
public class QuoteHistoryDAO extends GenericDAO<QuoteHistory> {

    public QuoteHistoryDAO() {
        super(QuoteHistory.class);
    }

    public List<QuoteHistory> getLastHour(User user) {
        Date minusHour = Tools.getDate(LocalDateTime.now().minusHours(1));

        return em.createQuery("SELECT q FROM QuoteHistory q WHERE q.dateTime >= :minusHour AND q.user = :user ORDER BY q.dateTime")
                .setParameter("minusHour", minusHour)
                .setParameter("user", user)
                .getResultList();
    }

    public Double getAverageQuote(User user) {
        return (Double) em.createQuery("SELECT AVG(q.quote) FROM QuoteHistory q WHERE q.user = :user")
                .setParameter("user", user)
                .getSingleResult();
    }

    public Double getAverageVariation(User user) {
        return (Double) em.createQuery("SELECT AVG(q.variation) FROM QuoteHistory q WHERE q.user = :user")
                .setParameter("user", user)
                .getSingleResult();
    }
}
