package montp.web;

import montp.data.model.security.User;
import montp.services.UserService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import java.io.Serializable;
import java.security.Principal;
import java.util.Arrays;

@SessionScoped
@Named
public class UserSession implements Serializable {

    @Inject
    private UserService userService;

    private User user;

    @PostConstruct
    public void init() {
        user = getFreshUser();
    }
    
    public void logout() {
        FacesTools.getRequest().getSession().invalidate();
        FacesTools.redirect("index");
    }

    public User getUser() { return user; }

    public User getFreshUser() {
        Principal p = FacesTools.getRequest().getUserPrincipal();

        if (p != null) {
            return userService.getFromUsername(p.getName());
        }

        return null;
    }

    public boolean userNameHasChanged(String username) {
        return !FacesTools.getRequest().getUserPrincipal().getName().equals(username);
    }
}
