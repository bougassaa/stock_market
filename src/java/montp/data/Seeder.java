package montp.data;

import montp.data.model.security.Group;
import montp.data.model.security.User;
import montp.services.UserService;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Singleton
@Startup
public class Seeder {
    
    @Inject
    private UserService userService;

    @PersistenceContext
    private EntityManager em;
    
    @PostConstruct
    public void init() {
        if (userService.getGroup("USER") == null) {
            Group groupUser = new Group("USER");
            em.persist(groupUser);
            Group groupAdmin = new Group("ADMIN");
            em.persist(groupAdmin);

            List<Group> group1 = new ArrayList<>();
            group1.add(groupUser);

            User userUser1 = new User("user1", "user1");
            userUser1.setGroups(group1);
            userService.insert(userUser1);

            List<Group> group2 = new ArrayList<>();
            group1.add(groupAdmin);

            User userAdmin = new User("admin", "admin");
            userAdmin.setGroups(group2);
            userService.insert(userAdmin);

            // todo : remove before release
            User amine = new User("abougassa@esimed.fr", "azerty");
            userService.insert(amine);
        }
    }

}
