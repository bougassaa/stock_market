package montp.web.controllers;

import montp.data.dao.GenericDAO;
import montp.data.model.GenericEntity;
import montp.services.GenericService;


public abstract class AbstractSingleObjectView<
        Entity extends GenericEntity,
        Dao extends GenericDAO<Entity>,
        Service extends GenericService<Entity, Dao>> extends AbstractView<Entity, Dao, Service> {

    protected Entity object;

    public Entity getObject() {
        return object;
    }

    public void setObject(Entity object) {
        this.object = object;
    }

}
