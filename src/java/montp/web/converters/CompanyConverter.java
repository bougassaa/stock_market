package montp.web.converters;

import montp.api.Company;
import montp.api.StockMarketClient;
import montp.tools.Tools;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

@Named
@FacesConverter(value = "companyConverter", forClass = Company.class, managed = true)
public class CompanyConverter implements Converter<Company> {

    StockMarketClient api;

    public CompanyConverter() {
        api = Tools.lookupBean(StockMarketClient.class);
    }

    @Override
    public Company getAsObject(FacesContext context, UIComponent component, String value) {
        return api.getCompany(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Company value) {
        return value.getSymbol();
    }

}
