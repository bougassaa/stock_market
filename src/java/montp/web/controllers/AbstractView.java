package montp.web.controllers;

import montp.data.dao.GenericDAO;
import montp.data.model.GenericEntity;
import montp.locale.Messages;
import montp.services.GenericService;
import montp.web.FacesTools;

import javax.inject.Inject;


public abstract class AbstractView<
        Entity extends GenericEntity,
        Dao extends GenericDAO<Entity>,
        Service extends GenericService<Entity, Dao>> extends View {

    @Inject protected Service service;
    @Inject protected Messages messages;

    public boolean canDelete(Entity object) {
        return service.canDelete(object);
    }

}
