package montp.data.model;

import montp.data.model.security.User;
import montp.tools.Tools;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "QUOTE_CART")
public class QuoteCart extends GenericEntity {
    
    private String companySymbol;
    private Double initialQuote;

    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;

    public QuoteCart() {
    }

    public QuoteCart(String companySymbol, Double initialQuote, User user) {
        this.companySymbol = companySymbol;
        this.initialQuote = initialQuote;
        this.user = user;
    }

    public User getUser() {
        return  user;
    }

    public void setUser(User  user) {
        this. user =  user;
    }

    public Double getInitialQuote() {
        return initialQuote;
    }

    public void setInitialQuote(Double quote) {
        this.initialQuote = quote;
    }

    public String getCompanySymbol() {
        return companySymbol;
    }

    public void setCompanySymbol(String company) {
        this.companySymbol = company;
    }

}
