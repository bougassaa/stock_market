package montp.web.controllers.dialogs;

import montp.data.dao.UserDAO;
import montp.data.model.security.User;
import montp.services.UserService;
import montp.web.FacesTools;
import montp.web.UserSession;
import montp.web.controllers.AbstractSingleObjectView;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@ViewScoped
@Named("editAccount")
public class EditAccountDialog extends AbstractSingleObjectView<User, UserDAO, UserService> {

    @Inject
    UserSession userSession;

    @PostConstruct
    public void init() {
        setObject(userSession.getUser());
    }

    public void save() {
        service.update(object);
        if (userSession.userNameHasChanged(object.getUserName())) {
            FacesTools.addFlashMessage(FacesMessage.SEVERITY_INFO, "Vous avez modifié votre mot de passe, veuillez vous connecter avec.");
            userSession.logout();
        } else {
            FacesTools.addMessage(FacesMessage.SEVERITY_INFO, "Compte à jour");
        }
    }

}
