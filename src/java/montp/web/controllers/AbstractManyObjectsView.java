package montp.web.controllers;

import montp.data.dao.GenericDAO;
import montp.data.model.GenericEntity;
import montp.services.GenericService;

import java.util.List;


public abstract class AbstractManyObjectsView<
        Entity extends GenericEntity,
        Dao extends GenericDAO<Entity>,
        Service extends GenericService<Entity, Dao>> extends AbstractView<Entity, Dao, Service> {

    protected List<Entity> objects;

    public List<Entity> getObjects() {
        return objects;
    }

    public void setObjects(List<Entity> objects) {
        this.objects = objects;
    }
}
