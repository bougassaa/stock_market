package montp.web.controllers;

import montp.data.model.security.User;
import montp.web.FacesTools;
import montp.web.UserSession;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@SessionScoped
@Named("superView")
public class View implements Serializable {

    @Inject
    private UserSession session;

    public boolean isLogged() {
        return FacesTools.getRequest().getUserPrincipal() != null;
    }

    public String getEmailPattern() {
        return "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    }

    public void redirect(String page) {
        FacesTools.redirect(page);
    }

    public void checkConnection() {
        if (!isUserActive()) {
            FacesTools.addFlashMessage(FacesMessage.SEVERITY_WARN, "Votre compte a été désactivé, contactez l'administrateur pour avoir plus d'informations.");
            session.logout();
        }
    }

    public boolean isUserActive() {
        if (isLogged()) {
            User u = session.getFreshUser();

            return u.getIsActive();
        }
        return true;
    }
}
