package montp.web.controllers.dialogs;

import montp.api.Company;
import montp.api.StockMarketClient;
import montp.data.dao.QuoteCartDAO;
import montp.data.model.QuoteCart;
import montp.services.QuoteCartService;
import montp.web.controllers.AbstractManyObjectsView;
import org.primefaces.PrimeFaces;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@ViewScoped
@Named("addCompanies")
public class AddCompaniesDialog extends AbstractManyObjectsView<QuoteCart, QuoteCartDAO, QuoteCartService> {

    @Inject
    StockMarketClient api;

    List<Company> selectedCompanies;

    public List<Company> searchCompanies(String query) {
        return (List<Company>) api.getCompanies(query);
    }

    public List<Company> getSelectedCompanies() {
        return selectedCompanies;
    }

    public void setSelectedCompanies(List<Company> selectedCompanies) {
        this.selectedCompanies = selectedCompanies;
    }

    public void save() {
        Set<String> symbols = new HashSet<>();
        // create array of symbols
        selectedCompanies.forEach(c -> symbols.add(c.getSymbol()));

        service.saveCart(symbols);

        // reset selected companies
        selectedCompanies.clear();

        // update front-end
        PrimeFaces.current().ajax().update("dashBoardForm");
        PrimeFaces.current().executeScript("PF('addCompanies').hide()");
        PrimeFaces.current().executeScript("initTextFit()");
    }
}
